// Request a node to be allocated to us
node( "SnapAMD64" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Make sure the local environment is cleaned up
		stage('Cleaning up') {
			// We don't want to use anything leftover from a prior run
			deleteDir()

			// Make sure any snap artifacts are gone too
			sh """
				rm -rf ~/snap/
			"""
		}

		// Retrieve the build instructions for the Snap
		stage('Fetching Sources') {
			// Actual Application Sources
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: branchToBuild]],
				extensions: [[$class: 'CloneOption', timeout: 120]],
				userRemoteConfigs: [[url: repositoryUrl]]
			]
		}

		// Build the application!
		stage('Building Application') {
			// Build it!
			sh """
				cd \$WORKSPACE/${snapDefinitionDirectory}/

				snapcraft

				rm -rf ~/snap/
			"""
		}

		// Capture the resulting *.snap files
		stage('Capturing Build') {
			archiveArtifacts artifacts: '*.snap', onlyIfSuccessful: true
		}

		// Cleanup on the way out
		deleteDir()
	}
}
}
